class Article < ApplicationRecord
  has_many :ratings, dependent: :destroy
  has_many :comments, dependent: :destroy
  validates :title, presence: true,
                    length: { minimum: 5 }
  validates :text, presence: true

  def self.to_csv
    columns_names = %w[title rating_min rating_max rating_avg comments_nb comments_characters_avg]

    CSV.generate do |csv|
      csv << columns_names

      all.each do |article|
        title = article.title
        rating_min = article.ratings.minimum(:score)
        rating_max = article.ratings.maximum(:score)
        rating_avg = article.ratings.average(:score)
        comments_nb = article.comments.count()
        comments_characters_avg = 0
        comments_characters = 0

        article.comments.each do |comment|
          comments_characters += comment.body.length
          comments_characters_avg = comments_characters/comments_nb
        end
        csv << [title, rating_min, rating_max, rating_avg, comments_nb, comments_characters_avg]
      end
    end
  end
end
